#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/util/datetime.h"
#include "sd_card.h"
#include "ff.h"
#include "lib/arducam/arducam.h"
#include "hardware/i2c.h"
#include "hardware/rtc.h"

#define MOTION_SENSOR_PIN 11

void setup_gpios(void);

uint8_t image_buf[324 * 324];
// uint8_t image_tmp[162 * 162];
uint8_t image_tmp[324 * 324];
uint8_t image[96 * 96];
uint8_t header[2] = {0x55, 0xAA};

FRESULT fr, fimg, fparams;
FATFS fs;
FIL fil;
int ret;
char buf[100];
char c[1000];
char imagenum_filename[] = "imagenum.txt";
char log_filename[] = "log.txt";
char tmpstr1[16];
char tmpstr2[16];
uint image_num = 0;
char current_log[200];
int tick_num = 0;
bool need_to_log = false;

char datetime_buf[256];
char *datetime_str = &datetime_buf[0];
datetime_t t = {
    .year = 2023,
    .month = 12,
    .day = 01,
    .dotw = 1, // 0 is Sunday, so 5 is Friday
    .hour = 00,
    .min = 00,
    .sec = 00};
static volatile bool fired = false;

// volatile bool timer_fired = false;


/**
 * Get RTC datetime
 */
char *get_datetime()
{
    rtc_get_datetime(&t);
    datetime_to_str(datetime_str, sizeof(datetime_buf), &t);
    return datetime_str;
}

int create_log(char *log_txt){
    // printf("\nTrying to create log : %s", log_txt);
     // ############### LOG FILE #####################
    // Open file for writing ()
    fr = f_open(&fil, log_filename, FA_WRITE | FA_OPEN_APPEND);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not open file (%d)\r\n", fr);
        while (true)
            ;
    }
    // Write something to file
    ret = f_printf(&fil, "%s\n", log_txt);
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }
    // Close file
    fr = f_close(&fil);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fr);
        while (true)
            ;
    }
    // finally, unmount SD card
    // f_unmount("0:");
}

// bool repeating_timer_callback(struct repeating_timer *t) {
//     need_to_log = true;
//     return true;
// }

static void alarm_callback(void) {

    // datetime_t t_alarm = {0};
    // rtc_get_datetime(&t_alarm);
    // char datetime_buf[256];
    // char *datetime_str = &datetime_buf[0];
    // datetime_to_str(datetime_str, sizeof(datetime_buf), &t_alarm);
    // printf("\nAlarm Fired At %s\n", datetime_str);
    need_to_log = true;
    stdio_flush();
    fired = true;
}

void setup_gpios(void)
{
    i2c_init(i2c1, 400000);
    gpio_set_function(2, GPIO_FUNC_I2C);
    gpio_set_function(3, GPIO_FUNC_I2C);
    gpio_pull_up(2);
    gpio_pull_up(3);
}


int save_image(uint8_t myimg[], uint width, uint height)
{
    char image_filename[20];
    sprintf(image_filename, "pic%u.pgm", image_num);
    image_num++;
    // Mount drive
    // fr = f_mount(&fs, "0:", 1);
    // if (fr != FR_OK)
    // {
    //     printf("ERROR: Could not mount filesystem (%d)\r\n", fr);
    //     while (true)
    //         ;
    // }
    // Open file for writing ()
    fimg = f_open(&fil, image_filename, FA_WRITE | FA_CREATE_ALWAYS);

    if (fimg != FR_OK)
    {
        printf("ERROR: Could not open file (%d / %s)\r\n", fimg, image_filename);
        while (true)
            ;
    }

    // Write image file
    ret = f_printf(&fil, "P2\n%u %u 255\n", width, height);
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            f_printf(&fil, "%u ", myimg[y * width + x]);
        }
        f_printf(&fil, "\n");
    }

    // Close file
    fimg = f_close(&fil);
    if (fimg != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fimg);
        while (true)
            ;
    }
    // update image num
    fparams = f_open(&fil, imagenum_filename, FA_WRITE | FA_CREATE_ALWAYS);

    if (fparams != FR_OK)
    {
        printf("ERROR: Could not open file (%d / %s)\r\n", fparams, imagenum_filename);
        while (true)
            ;
    }

    // Write params file
    ret = f_printf(&fil, "%d", image_num);
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }

    // Close file
    fparams = f_close(&fil);
    if (fparams != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fparams);
        while (true)
            ;
    }
    printf("Saved: %s\n", image_filename);
    // finally, unmount SD card
    // f_unmount("0:");

    printf("\n%s",get_datetime());    
    sprintf(current_log, "PHOTO : %d/%d/%d %dh %dmin %dsec", t.day, t.month, t.year, t.hour, t.min, t.sec);
    printf("%s", current_log);
    create_log(current_log);
    sprintf(current_log, "");
    
    return 0;
}

int main()
{
    // Initialize chosen serial port
    stdio_init_all();
    // RTC Alarm
    rtc_init();
    rtc_set_datetime(&t);
    sleep_us(64);
    datetime_t alarm = {
        .year  = -1,
        .month = -1,
        .day   = -1,
        .dotw  = -1,
        .hour  = -1,
        .min   = -1,
        .sec   = 00
    };
    rtc_set_alarm(&alarm, &alarm_callback);

    sleep_ms(1000);

    printf("\r\nStarting with PIR pin 11...\r\n");
    // Wait for user to press 'enter' to continue
    // printf("\r\nSD card test. Press 'enter' to start.\r\n");
    // while (true) {
    //     buf[0] = getchar();
    //     if ((buf[0] == '\r') || (buf[0] == '\n')) {
    //         break;
    //     }
    // }
    // Initialize cam
    struct arducam_config config;
    config.sccb = i2c0;
    config.sccb_mode = I2C_MODE_16_8;
    config.sensor_address = 0x24;
    config.pin_sioc = PIN_CAM_SIOC;
    config.pin_siod = PIN_CAM_SIOD;
    config.pin_resetb = PIN_CAM_RESETB;
    config.pin_xclk = PIN_CAM_XCLK;
    config.pin_vsync = PIN_CAM_VSYNC;
    config.pin_y2_pio_base = PIN_CAM_Y2_PIO_BASE;
    config.pio = pio0;
    config.pio_sm = 0;
    config.dma_channel = 0;
    config.image_buf = image_buf;
    config.image_buf_size = sizeof(image_buf);
    arducam_init(&config);
    uint16_t x, y, i, j, index;

    // Initialize SD card
    if (!sd_init_driver())
    {
        printf("ERROR: Could not initialize SD card\r\n");
        while (true)
            ;
    }

    // Mount drive
    fr = f_mount(&fs, "0:", 1);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not mount filesystem (%d)\r\n", fr);
        while (true)
            ;
    }

    // Log device start
    create_log("#### DEVICE IS STARTING ####");

    // ############### READ PARAMS FILE #####################

    // Open file for reading
    fr = f_open(&fil, imagenum_filename, FA_READ);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not open file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Print every line in file over serial
    printf("Reading params from '%s':\r\n", imagenum_filename);
    // printf("---\r\n");
    // while (f_gets(buf, sizeof(buf), &fil))
    // {
    //     printf("\n---\n");
    //     sscanf(buf, "%15s : %15[^;];", tmpstr1, tmpstr2);
    //     printf("%s => %d", tmpstr1, atoi(tmpstr2));
    //     if (strcmp(tmpstr1, "image_num") == 0)
    //     {
    //         image_num = atoi(tmpstr2);
    //     }
    //     else
    //     {
    //         printf("Unrecognized parameter : \"%s\"\n", tmpstr1);
    //     }
    // }
    // printf("\n---\n");

    f_gets(buf, sizeof(buf), &fil);
    image_num = atoi(buf);
    printf("Starting at image #%d\n", image_num);

    // Close file
    fr = f_close(&fil);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Unmount drive
    // f_unmount("0:");

    // timer
    // struct repeating_timer timer;
    // add_repeating_timer_ms(60000, repeating_timer_callback, NULL, &timer);

    while (true)
    {
        if (need_to_log == true) {
            tick_num++;
            sprintf(current_log, "[%d] %s", tick_num, get_datetime());
            printf("/n%s", current_log);
            create_log(current_log);
            need_to_log = false;
        }
        bool motion_detected = gpio_get(MOTION_SENSOR_PIN);
        if (motion_detected)
        {
            printf("\nMotion detected!\n");
            printf("[o] ");
            gpio_put(PIN_LED, true);
            arducam_capture_frame(&config);
            i = 0;
            index = 0;
            uint8_t temp = 0;
            for (y = 0; y < 324; y++)
            {
                for (x = (1 + x) % 2; x < 324; x += 2)
                {
                    image_tmp[index++] = config.image_buf[y * 324 + x];
                }
            }
            // Save image to microSD card
            save_image(image_buf, 324, 324);
            gpio_put(PIN_LED, false);
            sleep_ms(5000);
        }
        // return 0;
    }
    f_unmount("0:");
    return 0;
}
