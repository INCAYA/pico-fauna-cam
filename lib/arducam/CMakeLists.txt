# Set minimum required version of CMake
cmake_minimum_required(VERSION 3.12)

# Include build functions from Pico SDK
include($ENV{PICO_SDK_PATH}/external/pico_sdk_import.cmake)

# Set name of project (as PROJECT_NAME) and C/C++ standards
project(arducam C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

add_library(arducam INTERFACE)
target_sources(arducam INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/arducam.c
)
target_link_libraries(arducam INTERFACE
    hardware_dma
    hardware_i2c
    hardware_pio
    hardware_pwm
    pico_stdlib
)
