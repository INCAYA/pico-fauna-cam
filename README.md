# Pico Fauna Cam

A *Raspberry pico*-based low consumption trailcam for wildlife monitoring

**This project is still in the improvement phase, but you can find some useful code in it!**

## Install

Clone `no-OS-FatFS-SD-SPI-RPi-Pico` (microSD reader library) in `lib/` directory:

```bash
git clone git@github.com:carlk3/no-OS-FatFS-SD-SPI-RPi-Pico.git`
```

## Hardware

- Adafruit [Micro SD SPI or SDIO Card Breakout Board](https://www.adafruit.com/product/4682) (3V)
- Arducam [HM01B0 Monochrome QVGA SPI Camera Module](https://www.arducam.com/product/arducam-hm01b0-qvga-spi-camera-module-for-raspberry-pi-pico-2/) for Raspberry Pi Pico
- DFRobot [PIR Motion Sensor SEN0171](https://wiki.dfrobot.com/PIR_Motion_Sensor_V1.0_SKU_SEN0171)

## What's next?

- use interrupts to reduce energy consumption
- make a Wifi version to post pictures to your favorite web server

## Contributors

- Mohamed Abdulrahim
- Thomas Maziere (INCAYA)

## License

GNU GENERAL PUBLIC LICENSE (GPL) version 3


