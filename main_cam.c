#include <stdio.h>
#include "pico/stdlib.h"
#include "sd_card.h"
#include "ff.h"
#include "lib/arducam/arducam.h"
#include "hardware/i2c.h"

#include "lib/ssd1306/ssd1306.h"

void setup_gpios(void);

uint8_t image_buf[324 * 324];
uint8_t image_tmp[162 * 162];
uint8_t image[96 * 96];
uint8_t header[2] = {0x55, 0xAA};

FRESULT fr, fimg;
FATFS fs;
FIL fil;
int ret;
char buf[100];
char filename[] = "test_precam.txt";
uint image_num = 0;

void setup_gpios(void)
{
    i2c_init(i2c1, 400000);
    gpio_set_function(2, GPIO_FUNC_I2C);
    gpio_set_function(3, GPIO_FUNC_I2C);
    gpio_pull_up(2);
    gpio_pull_up(3);
}

int save_image(uint8_t myimg[], uint width, uint height)
{
    char image_filename[20];
    sprintf(image_filename, "pic%u.pgm", image_num);
    image_num++;

    // Mount drive
    fr = f_mount(&fs, "0:", 1);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not mount filesystem (%d)\r\n", fr);
        while (true)
            ;
    }
    // Open file for writing ()
    fimg = f_open(&fil, image_filename, FA_WRITE | FA_CREATE_ALWAYS);

    if (fimg != FR_OK)
    {
        printf("ERROR: Could not open file (%d / %s)\r\n", fimg, image_filename);
        while (true)
            ;
    }

    // Write image file
    ret = f_printf(&fil, "P2\n%u %u 255\n", width, height);
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            f_printf(&fil, "%u ", myimg[y * height + x]);
        }
        f_printf(&fil, "\n");
    }

    // Close file
    fimg = f_close(&fil);
    if (fimg != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fimg);
        while (true)
            ;
    }
    f_unmount("0:");
    return 0;
}

int main()
{
    // Initialize chosen serial port
    stdio_init_all();
    sleep_ms(5000);
    printf("\r\nStarting...\r\n");
    // Wait for user to press 'enter' to continue
    // printf("\r\nSD card test. Press 'enter' to start.\r\n");
    // while (true) {
    //     buf[0] = getchar();
    //     if ((buf[0] == '\r') || (buf[0] == '\n')) {
    //         break;
    //     }
    // }

    // Initialize cam
    struct arducam_config config;
    config.sccb = i2c0;
    config.sccb_mode = I2C_MODE_16_8;
    config.sensor_address = 0x24;
    config.pin_sioc = PIN_CAM_SIOC;
    config.pin_siod = PIN_CAM_SIOD;
    config.pin_resetb = PIN_CAM_RESETB;
    config.pin_xclk = PIN_CAM_XCLK;
    config.pin_vsync = PIN_CAM_VSYNC;
    config.pin_y2_pio_base = PIN_CAM_Y2_PIO_BASE;
    config.pio = pio0;
    config.pio_sm = 0;
    config.dma_channel = 0;
    config.image_buf = image_buf;
    config.image_buf_size = sizeof(image_buf);
    arducam_init(&config);
    uint16_t x, y, i, j, index;

    // OLED
    setup_gpios();
    ssd1306_t disp;
    const char *words[] = {"SSD1306", "DISPLAY", "DRIVER"};
    disp.external_vcc = false;
    ssd1306_init(&disp, 128, 64, 0x3C, i2c1);
    ssd1306_draw_square(&disp, 0, 0, 50, 50);
    // ssd1306_clear(&disp);
    // sleep_ms(2000);
    ssd1306_show(&disp);
    // ssd1306_deinit(&disp);
    // ssd1306_clear(&disp);
    // ssd1306_clear(&disp);
    // ssd1306_show(&disp);
    // sleep_ms(2000);
    // ssd1306_draw_square(&disp, 50, 0, 20, 10);
    // ssd1306_show(&disp);

    // ssd1306_draw_string(&disp, 8, 24, 2, "INCAYA");
    // ssd1306_show(&disp);

    // Initialize SD card
    if (!sd_init_driver())
    {
        printf("ERROR: Could not initialize SD card\r\n");
        while (true)
            ;
    }

    // Mount drive
    fr = f_mount(&fs, "0:", 1);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not mount filesystem (%d)\r\n", fr);
        while (true)
            ;
    }

    // Open file for writing ()
    fr = f_open(&fil, filename, FA_WRITE | FA_CREATE_ALWAYS);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not open file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Write something to file
    ret = f_printf(&fil, "Try to save fauna img\r\n");
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }
    ret = f_printf(&fil, "Oh yeah.\r\n");
    if (ret < 0)
    {
        printf("ERROR: Could not write to file (%d)\r\n", ret);
        f_close(&fil);
        while (true)
            ;
    }

    // Close file
    fr = f_close(&fil);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Open file for reading
    fr = f_open(&fil, filename, FA_READ);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not open file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Print every line in file over serial
    printf("Reading from file '%s':\r\n", filename);
    printf("---\r\n");
    while (f_gets(buf, sizeof(buf), &fil))
    {
        printf(buf);
    }
    printf("\r\n---\r\n");

    // Close file
    fr = f_close(&fil);
    if (fr != FR_OK)
    {
        printf("ERROR: Could not close file (%d)\r\n", fr);
        while (true)
            ;
    }

    // Unmount drive
    f_unmount("0:");

    while (true)
    {
        printf("[o] ");
        gpio_put(PIN_LED, true);
        arducam_capture_frame(&config);
        i = 0;
        index = 0;
        uint8_t temp = 0;
        for (y = 0; y < 324; y++)
        {
            for (x = (1 + x) % 2; x < 324; x += 2)
            {
                image_tmp[index++] = config.image_buf[y * 324 + x];
            }
        }
        index = 0;
        for (y = 33; y < 129; y++)
        {
            for (x = 33; x < 129; x++)
            {
                image[index++] = image_tmp[y * 324 + x];
            }
        }
        // uart_write_blocking(uart0, header, 2);
        //  uart_write_blocking(uart0, config.image_buf, config.image_buf_size);
        // uart_write_blocking(uart0, image, 96 * 96);
        save_image(image_buf, 324, 324);
        gpio_put(PIN_LED, false);
        sleep_ms(3000);
    }

    return 0;
}
